<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'home'
]);

// Authentication routes...
Route::get('iniciar-sesion', [
    'uses' => 'Auth\AuthController@getLogin',
    'as' => 'login'
]);
Route::post('iniciar-sesion', 'Auth\AuthController@postLogin');

Route::get('logout', [
    'uses' => 'Auth\AuthController@getLogout',
    'as' => 'logout'
]);

// Registration routes...
Route::get('registro', [
    'uses' => 'Auth\AuthController@getRegister',
    'as' => 'register'
]);
Route::post('registro', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api', 'namespace' => 'API'], function ()
{
	Route::group(['prefix' => 'v1'], function ()
	{
        require Config::get('generator.path_api_routes');
	});
});


Route::resource('users', 'UserController');

Route::get('users/{id}/delete', [
    'as' => 'users.delete',
    'uses' => 'UserController@destroy',
]);


Route::resource('aseguradoras', 'AseguradoraController');

Route::get('aseguradoras/{id}/delete', [
    'as' => 'aseguradoras.delete',
    'uses' => 'AseguradoraController@destroy',
]);


Route::resource('colectivos', 'ColectivoController');

Route::get('colectivos/{id}/delete', [
    'as' => 'colectivos.delete',
    'uses' => 'ColectivoController@destroy',
]);
