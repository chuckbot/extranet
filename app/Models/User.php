<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class User extends Model
{
    
	public $table = "users";
    

	public $fillable = [
	    "user",
		"name",
		"email",
		"department",
		"salt",
		"password",
		"act"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "user" => "string",
		"name" => "string",
		"email" => "string",
		"department" => "string",
		"salt" => "string",
		"password" => "string",
		"act" => "string"
    ];

	public static $rules = [
	    "user" => "required",
		"name" => "required",
		"email" => "required|unique:users",
		"department" => "required",
		"password" => "required",
		"act" => "required"
	];

}
