<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('user')->nullable();
			$table->string('name')->nullable();
			$table->string('email')->unique()->required();
			$table->string('department')->nullable();
			$table->enum('role', ['aseguradoras', 'analistaAltoCentro', 'analistaProveedor', 'proveedor', 'administrador']);
			$table->string('salt')->required();
			$table->string('password')->required();
			$table->string('act')->nullable();
            $table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
